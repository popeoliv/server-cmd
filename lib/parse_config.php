<?php

function parse_config($ini_file) {
  if(!file_exists($ini_file)) {
    return false;
  }

  return parse_ini_file($ini_file, true);
}