#!/usr/bin/env bash

host=$(hostname)
user=$(whoami)
date=$(date +%Y-%m-%d-%H-%M-%S)

# Install script for custom user configuration on server

# Backup existing files into .bak
mkdir -p ~/.dotfiles-backup/$date
cp ~/.bashrc ~/.dotfiles-backup/$date/.bashrc.bak
cp ~/.bash_profile ~/.dotfiles-backup/$date/.bash_profile.bak
cp ~/.vimrc ~/.dotfiles-backup/$date/.vimrc.bak

# Copy new files into place
cp .bashrc ~/.bashrc
cp .bash_profile ~/.bash_profile
cp .vimrc ~/.vimrc

# add local bin to path
echo "export PATH=$PATH:~/.local/bin" >> ~/.bashrc

